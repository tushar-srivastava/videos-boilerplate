const webpack = require('webpack');
const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const AssetsPlugin = require('assets-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const settings = require('../server/settings');
const dummyPlugin = function(){};

const PATHS = {
    build: path.resolve(__dirname + '/../', 'dist/client')
};

const extractBundleCSS = new ExtractTextPlugin('css/[name].[chunkhash].css');

module.exports = {
    devtool: settings.get("development") ? "sourcemap": undefined,
	context: path.resolve(__dirname, '..'),
	entry: {
		bundle: './client/index.jsx'
	},
	output: {
		path: PATHS.build,
		filename: 'js/[name].[chunkhash].js',
		publicPath: '/'
	},
	resolve: {
		extensions: ['.js', '.jsx'],
        modules: ["client/css", "node_modules"]
	},
	module: {
		rules: [
			{
                test: /\.jsx|\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                options: {
                    babelrc: false,
                    presets: [
                        ["env", {
                            "targets": {
                                "browsers": ["last 2 versions", "safari >= 7"]
                            },
                            "modules": false,
                            "loose": true
                        }],
                        "react",
                        "stage-2"
                    ]
                }
			},
            { test: /\.css$/, use: ExtractTextPlugin.extract({
                fallback: "style-loader",
                use: "css-loader"
            }),
				exclude: path.resolve(__dirname + '/../', 'client/css/components')
            },
            {
                test: /\.(css|less|styl|scss|sass|sss)/,
                loader: ['css-loader'],
                include: path.resolve(__dirname + '/../', 'client/css/components')
            }
		]
	},
	plugins: [
		new webpack.DefinePlugin({
			__CLIENT__: true,
			__SERVER__: false,
			__DEBUG__: settings.get('debug'),
            static_url: settings.get('static_url'),
            madiea_url: settings.get('media_url')

		}),
        new CleanWebpackPlugin(['client'], {
            root: __dirname + '/../dist/',
            verbose: true,
            dry: false,
            allowExternal: false
        }),
        new webpack.optimize.ModuleConcatenationPlugin(),
        settings.get("development") ? dummyPlugin: new webpack.optimize.UglifyJsPlugin({
            sourceMap: true,
            compress: {
                warnings: false,
                screw_ie8: false,
                drop_console: true,
                conditionals: true,
                unused: true,
                comparisons: true,
                sequences: true,
                dead_code: true,
                evaluate: true,
                join_vars: true,
                if_return: true
            },
            output: {
                comments: false
            }
        }),
        new AssetsPlugin({
			filename: "resources.json",
			path: __dirname + '/../server',
            prettyPrint: true
		}),
        new webpack.optimize.AggressiveMergingPlugin(),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'external',
            filename: 'js/external.[chunkhash].js',
            minChunks(module) {
                return module.context && module.context.indexOf('node_modules') >= 0;
            }
        }),
        extractBundleCSS,
        settings.get("development") ? dummyPlugin:new CompressionPlugin({
            asset: "[path].gz[query]",
            algorithm: "gzip",
            test: /\.js$|\.css$/,
            threshold: 10240,
            minRatio: 0.8
        })
	]
};
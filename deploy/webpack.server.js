const webpack = require('webpack');
const path = require('path');
const nodeExternals = require('webpack-node-externals');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const settings = require('../server/settings');
const dummyPlugin = function(){};

module.exports = {
    context: path.resolve(__dirname, '..'),
    entry: './server/www.js',
    target: 'node',
    output: {
        path: path.join(__dirname, '..', 'dist/server'),
        filename: 'index.js'
    },
    node: {
        __filename: false,
        __dirname: false,
    },
    resolve: {
        extensions: ['.js', '.jsx'],
        modules: ["client/css", "node_modules"]
    },
    externals: [nodeExternals()],
    plugins: [
        new webpack.DefinePlugin({
            __CLIENT__: false,
            __SERVER__: true,
            __DEBUG__: settings.get("debug")
        }),
        new CleanWebpackPlugin(['server'], {
            root: __dirname + '/../dist/',
            verbose: true,
            dry: false,
            allowExternal: false
        }),
        settings.get("development") ? dummyPlugin: new webpack.optimize.UglifyJsPlugin({
            sourceMap: true,
            compress: {
                warnings: false,
                screw_ie8: false,
                drop_console: true,
                conditionals: true,
                unused: true,
                comparisons: true,
                sequences: true,
                dead_code: true,
                evaluate: true,
                join_vars: true,
                if_return: true
            },
            output: {
                comments: false
            }
        }),
        new CopyWebpackPlugin([{
            from: path.join(__dirname, '..', 'server/templates'),
            to: path.join(__dirname, '..', 'dist/server/templates')
        }])
    ],
    module: {
        rules: [
            {
                test: /\.jsx|\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                options: {
                    babelrc: false,
                    presets: [
                        "env",
                        "react",
                        "stage-2"
                    ],
                    "plugins" : [[
                        "transform-runtime",
                        {
                            "polyfill": false,
                            "regenerator": true
                        }
                    ]]
                }
            },
            {
                test: /\.json$/,
                use: "json-loader"
            },
            {
                test: /\.(css|less|styl|scss|sass|sss)$/,
                use: ['isomorphic-style-loader', {
                    loader: 'css-loader',
                    options: {
                        importLoaders: 1
                    }
                }, 'sass-loader', ]
            }
        ]
    },
    devtool: 'sourcemap'
};
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { hydrate } from 'react-dom';
import PropTypes from 'prop-types';
import { BrowserRouter } from 'react-router-dom';
import createStore from './store';
//import styles from 'postload.css';
//import preloadStyles from 'components/preload.css';
import MainLayout from './components/layout'

const ContextType = {
    insertCss: PropTypes.func.isRequired
};

const context = {
    insertCss: () => {return;}
};

export default class App extends Component {
    static propTypes = {
        context: PropTypes.shape(ContextType).isRequired
    };

    static childContextTypes = ContextType;

    getChildContext() {
        return this.props.context;
    }

	render() {
        var store = createStore();
		return (
			<Provider store={store}>
                <BrowserRouter>
				    <MainLayout />
                </BrowserRouter>
			</Provider>
		);
	}
}

hydrate(
    <App context={context} />,
    document.getElementById('app')
);

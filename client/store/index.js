import { createStore } from 'redux';
import rootReducer from '../reducers';
//import createSagaMiddleware from 'redux-saga';

// function* rootSaga() {
//
// }

export default function(){
    var preloadedState = {
        user: false
    };
    if (__CLIENT__) {
        preloadedState = window.__PRELOADED_STATE__;
        delete window.__PRELOADED_STATE__;
    }
    const store = createStore(rootReducer, preloadedState);
    return store;
};

import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
//import StyledComponent from '../base/styled_component';
//import styles from 'components/preload.css';
import Test from './test';
import Home from './home';
//import postStyles from 'postload.css';


export default class MainLayout extends Component {
    render(){
        return  <div>
                    <Switch>
                        <Route exact path="/test" component={Test} />
                        <Route path="/" component={Home} />
                    </Switch>
                </div>;
    }
}


//export default withStyles(styles)(MainLayout);
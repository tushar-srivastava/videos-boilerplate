import React, { Component } from 'react';
import PropTypes from 'prop-types';

const ContextType = {
    // Enables critical path CSS rendering
    // https://github.com/kriasoft/isomorphic-style-loader
    insertCss: PropTypes.func.isRequired
};

export default class StyledComponent extends Component {

    static contextTypes = ContextType;
    constructor(props){
        super(props);
    }

    componentWillMount(){
        if(this.context.insertCss){
            this.context.insertCss(this._getStyles());
        }
    }

    getStyles(){
        throw new Error("No Overriden method getStyles");
    }

    _getStyles(){

        return this.getStyles();
    }
}
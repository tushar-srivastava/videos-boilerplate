export default function(state, action){
    switch(action.type){
        case "INIT":
            return Object.assign(state, {init: true});
        default:
            return state ||{};
    }
};
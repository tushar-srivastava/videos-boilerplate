
export default function(View){
    var handler = async function(ctx, next){
        const css = new Set();
        const context = {
            insertCss: (...styles) => {
                styles.forEach(style => css.add(style._getCss()));
            }
        };
        var view = new View(context, ctx.url);
        var html = await view.getAppHtml();
        var initialState = view.getInitialState();
        var cssString = [...css].join('').replace(/[\n\r\s]+/gm, '');
        await ctx.render(View.template, {
            html: html,
            criticalCss: cssString,
            initialState: JSON.stringify(initialState),
            scripts: ctx.resources.scripts,
            styleSheets: ctx.resources.styleSheets,
            static_url: ctx.resources.static_url
        });
    };

    return handler;
}
import { Bristol } from 'bristol'

export const logger = new Bristol();

/* istanbul ignore next */
if (process.env.LOG_LEVEL !== 'off') {
    logger.addTarget('console').withFormatter('human');;
    // logger.addTarget('console').withFormatter(palin, {
    //   rootFolderName: 'koa-es7-boilerplate' // Edit this to match your actual foldername
    // })
}

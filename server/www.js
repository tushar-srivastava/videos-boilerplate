import { createServer } from './index'
import { logger } from './logger'

console.log("Watching");

createServer().then(
    app =>{
        const PORT = process.env.PORT || 5000;
        const mode = process.env.NODE_ENV || "development";
        app.listen(PORT, () => {
            logger.debug(`Server listening on ${PORT} in ${mode} mode`)
        })},
    err => {
        logger.error('Error while starting up server', err);
        process.exit(1)
    }
);
import resources from '../resources.json';
import settings from '../settings';
const static_url = settings.get("static_url");
const scripts = [static_url + resources.external.js, static_url + resources.bundle.js];
const styleSheets = [static_url + resources.bundle.css];

const staticMiddleware  = async function(ctx, next){
    ctx.resources = {
        static_url: settings.get("static_url"),
        scripts: scripts,
        styleSheets: styleSheets
    };
    await next();
};

export default staticMiddleware;
import React from 'react';
import App from '../../main';
import ReactDOMServer from 'react-dom/server';
import createStore from '../../../client/store';

export default class BaseView {
    static template = 'index'
    
    constructor(context, url){
        this.context = context;
        this.url = url;
        this.store = createStore();
    }

    async getStoreWithData(store){
        return store;
    }

    getInitialState(){
        return this.store.getState();
    }
    async getAppHtml(){
        this.store = await this.getStoreWithData(this.store);
        return ReactDOMServer.renderToString(<App context={this.context} store={this.store} url={this.url}/>);;
    }
}
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { StaticRouter } from 'react-router';
import store from '../../client/store';
import MainLayout from '../../client/components/layout';

const ContextType = {
    // Enables critical path CSS rendering
    // https://github.com/kriasoft/isomorphic-style-loader
    insertCss: PropTypes.func.isRequired
};


export default class Main extends Component {

    static propTypes = {
        context: PropTypes.shape(ContextType).isRequired
    };

    static childContextTypes = ContextType;

    getChildContext() {
        return this.props.context;
    }

    render() {
        return (
            <Provider store={this.props.store}>
                <StaticRouter context={{}} location={this.props.url}>
                    <MainLayout/>
                </StaticRouter>
            </Provider>
        );
    }
}

import path from 'path';
import * as http from 'http';
import Koa from 'koa';
import respond from 'koa-respond';
import { logger } from './logger';
import hbs from 'koa-hbs';
import { errorHandler } from './middleware/error-handler';
import staticMiddleware from './middleware/static';
import router from './router';
import serve from 'koa-static';
import settings from './settings';

export async function createServer() {
    logger.debug('Creating server...');
    const app = new Koa();

    app.use(serve('dist/client'));
    app.use(staticMiddleware);
    // Container is configured with our services and whatnot.
    app.use(errorHandler)
        .use(respond());
    // Creates a http server ready to listen.
    const server = http.createServer(app.callback());
    app.use(
        hbs.middleware({
            viewPath: path.resolve(__dirname, 'templates'),
            disableCache: !settings.get("template_cache")
        })
    );
    app.use(router.routes()).use(router.allowedMethods());

    // Add a `close` event listener so we can clean up resources.
    server.on('close', () => {
        // Handler to tear down database connections, TCP connections, etc
        logger.debug('Server closing, bye!')
    });

    logger.debug('Server created, ready to listen', { scope: 'startup' });
    return server
}
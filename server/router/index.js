import Router from 'koa-router';
import viewHandler from '../handlers/view_handler';
import BaseView from '../views/base_view';

const router = new Router();
router.get('/test', viewHandler(BaseView));
router.get('/', viewHandler(BaseView));
export default router;
